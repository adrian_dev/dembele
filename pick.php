<?php
require_once "Draw.class.php";
$draw = new Draw();
$winner = $draw->pick();
$draw->award($winner["id"]);
header('Content-Type: application/json');
echo json_encode($winner);