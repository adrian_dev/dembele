<header class="row">
  <div class="column large-1 text-center small-4 large-offset-0 small-offset-4 medium-2 medium-offset-0">
    <img src="assets/mmoser-vr-logo.svg" />
  </div>
  <div class="column large-4 large-offset-7 medium-6 medium-offset-4">
    <img src="assets/logo.png" />
  </div>
</header>
