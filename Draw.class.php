<?php
class Draw
{
    private $db;

    public function __construct()
    {
        $this->db = new mysqli("localhost", "root", "MM0ser1981", "samsung_draw");

        if ($this->db->connect_errno > 0) {
            die("Unable to connect to database [{$this->db->connect_error}]");
        }
    }

    public function set()
    {
        if (!array_key_exists("name", $_POST) || empty($_POST["name"])) {
            return "Invalid name";
        }

        if (!array_key_exists("email", $_POST) || !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            return "Invalid email";
        }

        $name  = $_POST["name"];
        $email = $_POST["email"];

        $q      = "SELECT `id` FROM `entries` WHERE LOWER(`name`) LIKE LOWER('{$name}');";
        $result = $this->db->query($q);
        if ($result->num_rows > 0) {
            return "Name already exists";
        }

        $q      = "SELECT `id` FROM `entries` WHERE LOWER(`email`) LIKE LOWER('{$email}');";
        $result = $this->db->query($q);
        if ($result->num_rows > 0) {
            return "Email already exists";
        }

        $q = "INSERT INTO `entries` (`name`, `email`) VALUES ('{$name}', '{$email}');";

        $this->db->query($q);
        $id = $this->db->insert_id;
        return $id;
    }

    public function get($id = 0)
    {
        if ($id == 0 && (array_key_exists("id", $_POST) && is_int($_GET["id"]))) {
            $id = $_GET["id"];
        }

        $q = "SELECT * FROM `entries` WHERE `id` = '{$id}'";
        if ($result = $this->db->query($q)) {
            return $result->fetch_assoc();
        } else {
            return "Entry does not exist";
        }
    }

    public function all()
    {
        $q = "SELECT * FROM `entries` ORDER BY `id` DESC;";
        $result = $this->db->query($q);
        $entries = array();
        while ($row = $result->fetch_assoc()) {
            $entries[] = $row;
        }
        header('Content-Type: application/json');
        return json_encode($entries);
    }

    public function pick()
    {
        $winners = array(0);
        $q = "SELECT `entry_id` FROM `winners`;";
        $result = $this->db->query($q);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $winners[] = $row["entry_id"];
            }
        }
        $q = "SELECT * FROM `entries` WHERE `id` NOT IN (".implode(", ", $winners).") ORDER BY RAND() LIMIT 1;";
        $result = $this->db->query($q);
        if ($result && $result->num_rows > 0) {
            return $result->fetch_assoc();
        } else {
            echo $q;
        }
    }

    public function award($id = 0)
    {
        if ($id < 1) {
            return false;
        }

        $q = "SELECT `id` FROM `winners` WHERE `entry_id` = '{$id}';";
        $result = $this->db->query($q);
        if ($result->num_rows > 0) {
            return false;
        }

        $q = "INSERT INTO `winners` (`entry_id`) VALUES ('{$id}');";
        $this->db->query($q);
        $id = $this->db->insert_id;
        return $id;
    }

    public function lockin()
    {
        if (array_key_exists("total", $_GET) && $_GET["total"] > 0) {
            $total = $_GET["total"];
            $q = "SELECT `quantity` FROM `draws`";
            $result = $this->db->query($q);

            if ($result->num_rows > 0) {
                $q = "UPDATE `draws` SET `quantity` = '{$total}';";
            } else {
                $q = "INSERT INTO `draws` (`quantity`) VALUES ('{$total}');";
            }

            $this->db->query($q);
            $q = "SELECT `quantity` FROM `draws`";
            $result = $this->db->query($q);
            $record = $result->fetch_assoc();
            return $record["quantity"];
        }

        return false;
    }

    public function find()
    {
        if (!array_key_exists("name", $_POST) || empty($_POST["name"])) {
            return "Invalid name";
        }

        if (!array_key_exists("email", $_POST) || !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            return "Invalid email";
        }

        $name  = $_POST["name"];
        $email = $_POST["email"];

        $q      = "SELECT `id` FROM `entries` WHERE LOWER(`name`) LIKE LOWER('{$name}');";
        $result = $this->db->query($q);
        if ($result->num_rows > 0) {
            return "Name already exists";
        }

        $q      = "SELECT `id` FROM `entries` WHERE LOWER(`email`) LIKE LOWER('{$email}');";
        $result = $this->db->query($q);
        if ($result->num_rows > 0) {
            return "Email already exists";
        }

        return false;
    }

    public function winners()
    {
        $q = "SELECT `entry_id` FROM `winners`";
        $result = $this->db->query($q);
        $winners = array();
        while ($winner = $result->fetch_assoc()) {
            $id = $winner["entry_id"];
            $q = "SELECT * FROM `entries` WHERE `id` = '{$id}';";
            $entry = $this->db->query($q);
            $winners[] = $entry->fetch_assoc();
        }
        return $winners;
    }

    public function totalDraws()
    {
      $result = $this->db->query("SELECT `quantity` FROM `draws`");
      $result = $result->fetch_assoc();
      if (!empty($result)) {
        return $result['quantity'];
      } else {
        return 0;
      }
    }

    public function totalEntries()
    {
      $result = $this->db->query("SELECT COUNT(`id`) AS total FROM `entries`");
      $result = $result->fetch_assoc();

      if (!empty($result)) {
        return $result['total'];
      } else {
        return 0;
      }
    }
}
