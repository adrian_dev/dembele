<?php
if ( ! session_id() ) @ session_start();

$err = '';

if (isset($_POST["sub-mission"])) {
  if (isset($_POST["username"]) && !empty($_POST["username"])) {
    if (sha1($_POST["username"]) == "575a910a8aad0aaded70e59f35905a9f721d080d") {
      if (isset($_POST["password"]) && !empty($_POST["password"])) {
        if (sha1($_POST["password"]) == "d6fa3138cf5041eeac943454f457bf3ca6eaaf16") {
          $_SESSION["sgvrd"] = true;
        } else {
          $err = '<div style="color:red;">username or password mismatch</div>';
        }
      } else {
        $err = '<div style="color:red;">username or password mismatch</div>';
      }
    } else {
      $err = '<div style="color:red;">username or password mismatch</div>';
    }
  } else {
    $err = '<div style="color:red;">username or password mismatch</div>';
  }
}

if (!isset($_SESSION["sgvrd"])) {
  ?>
  <!DOCTYPE html>
    <?php require_once "head.php"; ?>
    <body>
      <?php require_once "header.php"; ?>
      <form method="post" action="">
        <?php echo $err; ?>
        <input type="text" name="username" placeholder="username" maxlength="100" required />
        <input type="password" name="password" placeholder="password" maxlength="100" required />
        <input type="submit" name="sub-mission" value="LOGIN" class="button hollow secondary expanded" />
      </form>
    </body>
  <?php
  die();
}

require_once "Draw.class.php";

$draw    = new Draw();
$winners = $draw->winners();
$won     = count($winners);
$entries = $draw->totalEntries();
$draws   = $draw->totalDraws();
?>

<!DOCTYPE html>
<html>
  <?php require_once "head.php" ?>
  <body>
    <?php require_once "header.php";?>

    <main>
      <?php if (($draws - $won) > 0) :?>
        <div class="row text-center" id="prize-input">
          <h4 class="column">Enter the total number of winners to be picked:</h4>
          <div class="column large-2 large-offset-5">
            <input type="number" id="total" class="text-center" />
          </div>
          <div class="column large-2 large-offset-5 end">
            <button id="set" class="button expanded">LOCK IN</button>
          </div>

          <?php if ($won > 0):?>
            <div class="column large-6 large-offset-3 end text-center">
              <?php echo $won; ?> winner/s have already been picked. Do you want to skip this part?
              <a href="javascript:void(0);" id="skip">YES</a>
            </div>
          <?php endif;?>

          <?php if ($entries < 1):?>
            <div class="column text-center">There are still no entries.</div>
          <?php endif;?>
        </div>
      <?php else:?>
        <div class="column text-center">Winners have already been picked</div>
      <?php endif;?>

      <div class="row" id="pick-winner">
        <div class="column large-6 large-offset-3 picky-picky">
          <button id="draw" class="button expanded large" disabled>Pick a WINNER</button>
        </div>

        <div class="column">
          <ol id="winners" class="row"></ol>
        </div>

        <audio id="drumroll" src="assets/drumroll.wav" preload="auto"></audio>
      </div>
    </main>

    <script>
      var picking = false;
      var maximum = 0;
      var winners = new Array();
      var entries = <?php echo $entries; ?>;
      var drawn = <?php echo $won; ?>;

      $(function() {
        var drum = $('#drumroll');

        if (entries < 1) {
          $('#set').hide();
        }

        drum.on('canplaythrough', function() {
          var btn = $('#draw');
          btn.prop('disabled', false);

          btn.click(function() {
            if (!picking) {
              btn.hide();
              picking = true;

              if ($("#winners").children().length == 0) {
                $("#winners").prepend('<li class="column large-6 large-offset-3 end text-center"><span>And the winner is...</span></li>');
              } else {
                $("#winners li:first-of-type").html("<span>And the winner is...</span>");
              }

              $("#winners li:first-of-type").addClass("shaky");

              $.ajax({
                url: 'pick.php',
                success: function(response) {
                  drum.get(0).play();
                  $("#winners li").removeClass("first");

                  setTimeout(function() {
                    $("#winners li:first-of-type").text(response.name);
                    $("#winners li:first-of-type").addClass('first');
                    $("#winners li:first-of-type").removeClass('shaky');

                    winners.push(response.name);
                    picking = false;

                    if (winners.length >= maximum) {
                      btn.hide();
                    } else {
                      btn.delay(500).fadeIn();
                    }

                    setTimeout(function() {
                      $("#winners").prepend('<li class="column large-6 large-offset-3 text-center"></li>');
                      $("#winners li").removeClass("first");

                      if (winners.length >= maximum) {
                        btn.hide();
                        $("#winners li:first-of-type").hide();
                      }
                    }, 2000);
                  }, 4350);
                }
              });
            }
          });
        });

        $("#set").click(function() {
            if ($("#total").val() > 0) {
              $.ajax({
                url: 'lockin.php',
                type: 'get',
                data: {total:$("#total").val()},
                success: function(response) {
                  if (response > 0) {
                    $("#prize-input").slideUp();
                    $("#pick-winner").slideDown();
                    maximum = parseInt(response);
                  }
                }
              });
            }
          });

        $("#skip").click(function() {
          $("#prize-input").slideUp();
          $("#pick-winner").slideDown();
        });
      });
    </script>
  </body>
</html>
