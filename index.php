<?php

require_once "Draw.class.php";

$draw = new Draw();
$winners = $draw->winners();

$error = array_key_exists("error", $_GET) ? $_GET["error"] : "";
$name  = array_key_exists("name", $_GET) ? $_GET["name"] : "";
$email = array_key_exists("email", $_GET) ? $_GET["email"] : "";
?>
<!DOCTYPE html>
<html>
  <?php require_once "head.php";?>
  <body>
    <?php require_once "header.php";?>

    <main class="row">
      <?php if (!empty($winners)):?>
        <div class="column"><b>Notice:</b> Please empty the winners and entries table before starting registration.</div>
      <?php else:?>
        <div class="column large-6 large-offset-1">
          <h4>成功注册就有机会赢得套装:</h4>
          <h4>Register below and stand a chance to win:<br/><strong>Samsung Galaxy S6<br />& Samsung Gear VR Headset</strong></h4>
        </div>
        <div class="column large-6 small-12 large-offset-1 large-draw-date">
          <h5>35周年活动尾声将会公布得奖者。</h5>
          <h5>Winner will be announced the end of the PRD 35 event.</h5>
        </div>
        <div class="column large-6 small-12 large-offset-1">
          <div class="error"><?php echo $error ?></div>
        </div>

        <form method="post" action="save.php" id="register">
          <div class="column large-6 small-12 large-offset-1">
            <input type="text" name="name" id="name" placeholder="NAME" value="<?php echo $name ?>" required maxlength="150" />
            <input type="email" name="email" id="email" placeholder="EMAIL" value="<?php echo $email ?>" required maxlength="150" />
          </div>
        </form>

        <div class="column large-6 small-12 large-offset-1">
          <a href="javascript:void(0);" id="submit" class="button expanded">REGISTER NOW</a>
        </div>
        <div class="column large-6 small-12 large-offset-1 small-draw-date">
          <h4>35周年活动尾声将会公布得奖者。</h4>
          <h5>Winner will be announced the end of the PRD 35 event.</h5>
        </div>
        <div class="column">&nbsp;</div>
        <div class="column small-8 small-offset-2 end mini-logo text-center"><img src="assets/logo-mobile.png" /></div>
      <?php endif;?>
    </main>

    <script>
      $(function() {
        $('#submit').click(function() {
          $.ajax({
            url: "exists.php",
            type: "post",
            data: {
              name: $("#name").val(),
              email: $("#email").val()
            },
            success: function(response) {
              if (response.length == 0) {
                $("#register").submit();
              } else {
                $(".error").text(response);
                $("#name, #email").focus(function() {
                  $(".error").text("");
                });
              }
            }
          });
        });

        $('#email').keypress(function (e) {
          var key = e.which;

          if (key == 13) {
            $('#submit').trigger('click');
            return false;
          }
        });
      });
        </script>
    </body>
</html>
