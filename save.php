<?php
require_once "Draw.class.php";
$draw = new Draw();
$result = $draw->set();

if (is_numeric($result)) {
    $entry = $draw->get($result);
    header("Location: success.php?".http_build_query($entry));
} else {
    $query = array(
        "error" => $result,
        "name" => $_POST["name"],
        "email" => $_POST["email"]
    );
    header("Location: index.php?".http_build_query($query));
}