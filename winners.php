<?php
require_once "Draw.class.php";

$draw = new Draw();
$winners = $draw->winners();
?>
<!DOCTYPE html>
<html>
    <?php require_once "head.php"; ?>
    <body>
        <?php require_once "header.php";?>
        <main class="row medium-8">
            <div class="column">
                <table class="stack" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="300">Name</th><th width="300">Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($winners as $winner) :?>
                        <tr><td><?php echo $winner["name"]?></td><td><?php echo $winner["email"]?></td></tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </main>
    </body>
</html>